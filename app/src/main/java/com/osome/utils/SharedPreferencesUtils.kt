package com.osome.utils

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.google.gson.Gson
import com.osome.data.User
import de.adorsys.android.securestoragelibrary.SecurePreferences


class SharedPreferencesUtils(private var context: Context) {

    companion object {
        val AUTH_STATE = "auth_state"
        val TOKEN = "token"
        val USER = "user"
        val NAME = "osome"

        fun saveUser(user: User) {
            val gson = Gson()
            val json = gson.toJson(user)
            SecurePreferences.setValue(USER, json)
        }

        fun loadUser(): User? {
            val gson = Gson()
            val stringValue = SecurePreferences.getStringValue(USER, "")
            return if (!TextUtils.isEmpty(stringValue))
                gson.fromJson<User>(stringValue, User::class.java)
            else
                null
        }
    }

    fun getSharedPreferences() : SharedPreferences {
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE)
    }

    fun getSharedPreferencesForEdit(context: Context) : SharedPreferences.Editor {
        return context.getSharedPreferences(NAME, Context.MODE_PRIVATE).edit()
    }

}