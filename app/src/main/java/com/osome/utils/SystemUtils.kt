package com.osome.utils

import android.content.Context
import android.view.View
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager


class SystemUtils {
    companion object {

        fun hideKeyboard (view : View, context: Context) {
            if (view != null) {
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(view!!.getWindowToken(), 0)
            }
        }
    }
}