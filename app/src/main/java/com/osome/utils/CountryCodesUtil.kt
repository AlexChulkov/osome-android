package com.osome.utils

import android.text.TextUtils
import com.osome.R
import java.util.*

class CountryCodesUtil {

    companion object {
        fun getCountryCodeToRegionCodeMap(): Map<Int, List<String>> {
            // The capacity is set to 286 as there are 215 different entries,
            // and this offers a load factor of roughly 0.75.
            val countryCodeToRegionCodeMap = HashMap<Int, List<String>>(286)

            var listWithRegionCode: ArrayList<String>

            listWithRegionCode = ArrayList(25)
            listWithRegionCode.add("US")
            listWithRegionCode.add("AG")
            listWithRegionCode.add("AI")
            listWithRegionCode.add("AS")
            listWithRegionCode.add("BB")
            listWithRegionCode.add("BM")
            listWithRegionCode.add("BS")
            listWithRegionCode.add("CA")
            listWithRegionCode.add("DM")
            listWithRegionCode.add("DO")
            listWithRegionCode.add("GD")
            listWithRegionCode.add("GU")
            listWithRegionCode.add("JM")
            listWithRegionCode.add("KN")
            listWithRegionCode.add("KY")
            listWithRegionCode.add("LC")
            listWithRegionCode.add("MP")
            listWithRegionCode.add("MS")
            listWithRegionCode.add("PR")
            listWithRegionCode.add("SX")
            listWithRegionCode.add("TC")
            listWithRegionCode.add("TT")
            listWithRegionCode.add("VC")
            listWithRegionCode.add("VG")
            listWithRegionCode.add("VI")
            countryCodeToRegionCodeMap[1] = listWithRegionCode

            listWithRegionCode = ArrayList(2)
            listWithRegionCode.add("RU")
            listWithRegionCode.add("KZ")
            countryCodeToRegionCodeMap[7] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("EG")
            countryCodeToRegionCodeMap[20] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("ZA")
            countryCodeToRegionCodeMap[27] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GR")
            countryCodeToRegionCodeMap[30] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NL")
            countryCodeToRegionCodeMap[31] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BE")
            countryCodeToRegionCodeMap[32] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("FR")
            countryCodeToRegionCodeMap[33] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("ES")
            countryCodeToRegionCodeMap[34] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("HU")
            countryCodeToRegionCodeMap[36] = listWithRegionCode

            listWithRegionCode = ArrayList(2)
            listWithRegionCode.add("IT")
            listWithRegionCode.add("VA")
            countryCodeToRegionCodeMap[39] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("RO")
            countryCodeToRegionCodeMap[40] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CH")
            countryCodeToRegionCodeMap[41] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AT")
            countryCodeToRegionCodeMap[43] = listWithRegionCode

            listWithRegionCode = ArrayList(4)
            listWithRegionCode.add("GB")
            listWithRegionCode.add("GG")
            listWithRegionCode.add("IM")
            listWithRegionCode.add("JE")
            countryCodeToRegionCodeMap[44] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("DK")
            countryCodeToRegionCodeMap[45] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SE")
            countryCodeToRegionCodeMap[46] = listWithRegionCode

            listWithRegionCode = ArrayList(2)
            listWithRegionCode.add("NO")
            listWithRegionCode.add("SJ")
            countryCodeToRegionCodeMap[47] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PL")
            countryCodeToRegionCodeMap[48] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("DE")
            countryCodeToRegionCodeMap[49] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PE")
            countryCodeToRegionCodeMap[51] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MX")
            countryCodeToRegionCodeMap[52] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CU")
            countryCodeToRegionCodeMap[53] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AR")
            countryCodeToRegionCodeMap[54] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BR")
            countryCodeToRegionCodeMap[55] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CL")
            countryCodeToRegionCodeMap[56] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CO")
            countryCodeToRegionCodeMap[57] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("VE")
            countryCodeToRegionCodeMap[58] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MY")
            countryCodeToRegionCodeMap[60] = listWithRegionCode

            listWithRegionCode = ArrayList(3)
            listWithRegionCode.add("AU")
            listWithRegionCode.add("CC")
            listWithRegionCode.add("CX")
            countryCodeToRegionCodeMap[61] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("ID")
            countryCodeToRegionCodeMap[62] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PH")
            countryCodeToRegionCodeMap[63] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NZ")
            countryCodeToRegionCodeMap[64] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SG")
            countryCodeToRegionCodeMap[65] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TH")
            countryCodeToRegionCodeMap[66] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("JP")
            countryCodeToRegionCodeMap[81] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("KR")
            countryCodeToRegionCodeMap[82] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("VN")
            countryCodeToRegionCodeMap[84] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CN")
            countryCodeToRegionCodeMap[86] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TR")
            countryCodeToRegionCodeMap[90] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("IN")
            countryCodeToRegionCodeMap[91] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PK")
            countryCodeToRegionCodeMap[92] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AF")
            countryCodeToRegionCodeMap[93] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("LK")
            countryCodeToRegionCodeMap[94] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MM")
            countryCodeToRegionCodeMap[95] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("IR")
            countryCodeToRegionCodeMap[98] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SS")
            countryCodeToRegionCodeMap[211] = listWithRegionCode

            listWithRegionCode = ArrayList(2)
            listWithRegionCode.add("MA")
            listWithRegionCode.add("EH")
            countryCodeToRegionCodeMap[212] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("DZ")
            countryCodeToRegionCodeMap[213] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TN")
            countryCodeToRegionCodeMap[216] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("LY")
            countryCodeToRegionCodeMap[218] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GM")
            countryCodeToRegionCodeMap[220] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SN")
            countryCodeToRegionCodeMap[221] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MR")
            countryCodeToRegionCodeMap[222] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("ML")
            countryCodeToRegionCodeMap[223] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GN")
            countryCodeToRegionCodeMap[224] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CI")
            countryCodeToRegionCodeMap[225] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BF")
            countryCodeToRegionCodeMap[226] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NE")
            countryCodeToRegionCodeMap[227] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TG")
            countryCodeToRegionCodeMap[228] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BJ")
            countryCodeToRegionCodeMap[229] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MU")
            countryCodeToRegionCodeMap[230] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("LR")
            countryCodeToRegionCodeMap[231] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SL")
            countryCodeToRegionCodeMap[232] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GH")
            countryCodeToRegionCodeMap[233] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NG")
            countryCodeToRegionCodeMap[234] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TD")
            countryCodeToRegionCodeMap[235] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CF")
            countryCodeToRegionCodeMap[236] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CM")
            countryCodeToRegionCodeMap[237] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CV")
            countryCodeToRegionCodeMap[238] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("ST")
            countryCodeToRegionCodeMap[239] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GQ")
            countryCodeToRegionCodeMap[240] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GA")
            countryCodeToRegionCodeMap[241] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CG")
            countryCodeToRegionCodeMap[242] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CD")
            countryCodeToRegionCodeMap[243] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AO")
            countryCodeToRegionCodeMap[244] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GW")
            countryCodeToRegionCodeMap[245] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("IO")
            countryCodeToRegionCodeMap[246] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AC")
            countryCodeToRegionCodeMap[247] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SC")
            countryCodeToRegionCodeMap[248] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SD")
            countryCodeToRegionCodeMap[249] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("RW")
            countryCodeToRegionCodeMap[250] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("ET")
            countryCodeToRegionCodeMap[251] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SO")
            countryCodeToRegionCodeMap[252] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("DJ")
            countryCodeToRegionCodeMap[253] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("KE")
            countryCodeToRegionCodeMap[254] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TZ")
            countryCodeToRegionCodeMap[255] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("UG")
            countryCodeToRegionCodeMap[256] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BI")
            countryCodeToRegionCodeMap[257] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MZ")
            countryCodeToRegionCodeMap[258] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("ZM")
            countryCodeToRegionCodeMap[260] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MG")
            countryCodeToRegionCodeMap[261] = listWithRegionCode

            listWithRegionCode = ArrayList(2)
            listWithRegionCode.add("RE")
            listWithRegionCode.add("YT")
            countryCodeToRegionCodeMap[262] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("ZW")
            countryCodeToRegionCodeMap[263] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NA")
            countryCodeToRegionCodeMap[264] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MW")
            countryCodeToRegionCodeMap[265] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("LS")
            countryCodeToRegionCodeMap[266] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BW")
            countryCodeToRegionCodeMap[267] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SZ")
            countryCodeToRegionCodeMap[268] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("KM")
            countryCodeToRegionCodeMap[269] = listWithRegionCode

            listWithRegionCode = ArrayList(2)
            listWithRegionCode.add("SH")
            listWithRegionCode.add("TA")
            countryCodeToRegionCodeMap[290] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("ER")
            countryCodeToRegionCodeMap[291] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AW")
            countryCodeToRegionCodeMap[297] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("FO")
            countryCodeToRegionCodeMap[298] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GL")
            countryCodeToRegionCodeMap[299] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GI")
            countryCodeToRegionCodeMap[350] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PT")
            countryCodeToRegionCodeMap[351] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("LU")
            countryCodeToRegionCodeMap[352] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("IE")
            countryCodeToRegionCodeMap[353] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("IS")
            countryCodeToRegionCodeMap[354] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AL")
            countryCodeToRegionCodeMap[355] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MT")
            countryCodeToRegionCodeMap[356] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CY")
            countryCodeToRegionCodeMap[357] = listWithRegionCode

            listWithRegionCode = ArrayList(2)
            listWithRegionCode.add("FI")
            listWithRegionCode.add("AX")
            countryCodeToRegionCodeMap[358] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BG")
            countryCodeToRegionCodeMap[359] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("LT")
            countryCodeToRegionCodeMap[370] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("LV")
            countryCodeToRegionCodeMap[371] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("EE")
            countryCodeToRegionCodeMap[372] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MD")
            countryCodeToRegionCodeMap[373] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AM")
            countryCodeToRegionCodeMap[374] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BY")
            countryCodeToRegionCodeMap[375] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AD")
            countryCodeToRegionCodeMap[376] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MC")
            countryCodeToRegionCodeMap[377] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SM")
            countryCodeToRegionCodeMap[378] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("UA")
            countryCodeToRegionCodeMap[380] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("RS")
            countryCodeToRegionCodeMap[381] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("ME")
            countryCodeToRegionCodeMap[382] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("XK")
            countryCodeToRegionCodeMap[383] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("HR")
            countryCodeToRegionCodeMap[385] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SI")
            countryCodeToRegionCodeMap[386] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BA")
            countryCodeToRegionCodeMap[387] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MK")
            countryCodeToRegionCodeMap[389] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CZ")
            countryCodeToRegionCodeMap[420] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SK")
            countryCodeToRegionCodeMap[421] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("LI")
            countryCodeToRegionCodeMap[423] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("FK")
            countryCodeToRegionCodeMap[500] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BZ")
            countryCodeToRegionCodeMap[501] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GT")
            countryCodeToRegionCodeMap[502] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SV")
            countryCodeToRegionCodeMap[503] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("HN")
            countryCodeToRegionCodeMap[504] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NI")
            countryCodeToRegionCodeMap[505] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CR")
            countryCodeToRegionCodeMap[506] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PA")
            countryCodeToRegionCodeMap[507] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PM")
            countryCodeToRegionCodeMap[508] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("HT")
            countryCodeToRegionCodeMap[509] = listWithRegionCode

            listWithRegionCode = ArrayList(3)
            listWithRegionCode.add("GP")
            listWithRegionCode.add("BL")
            listWithRegionCode.add("MF")
            countryCodeToRegionCodeMap[590] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BO")
            countryCodeToRegionCodeMap[591] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GY")
            countryCodeToRegionCodeMap[592] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("EC")
            countryCodeToRegionCodeMap[593] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GF")
            countryCodeToRegionCodeMap[594] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PY")
            countryCodeToRegionCodeMap[595] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MQ")
            countryCodeToRegionCodeMap[596] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SR")
            countryCodeToRegionCodeMap[597] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("UY")
            countryCodeToRegionCodeMap[598] = listWithRegionCode

            listWithRegionCode = ArrayList(2)
            listWithRegionCode.add("CW")
            listWithRegionCode.add("BQ")
            countryCodeToRegionCodeMap[599] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TL")
            countryCodeToRegionCodeMap[670] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NF")
            countryCodeToRegionCodeMap[672] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BN")
            countryCodeToRegionCodeMap[673] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NR")
            countryCodeToRegionCodeMap[674] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PG")
            countryCodeToRegionCodeMap[675] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TO")
            countryCodeToRegionCodeMap[676] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SB")
            countryCodeToRegionCodeMap[677] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("VU")
            countryCodeToRegionCodeMap[678] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("FJ")
            countryCodeToRegionCodeMap[679] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PW")
            countryCodeToRegionCodeMap[680] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("WF")
            countryCodeToRegionCodeMap[681] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("CK")
            countryCodeToRegionCodeMap[682] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NU")
            countryCodeToRegionCodeMap[683] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("WS")
            countryCodeToRegionCodeMap[685] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("KI")
            countryCodeToRegionCodeMap[686] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NC")
            countryCodeToRegionCodeMap[687] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TV")
            countryCodeToRegionCodeMap[688] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PF")
            countryCodeToRegionCodeMap[689] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TK")
            countryCodeToRegionCodeMap[690] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("FM")
            countryCodeToRegionCodeMap[691] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MH")
            countryCodeToRegionCodeMap[692] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("001")
            countryCodeToRegionCodeMap[800] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("001")
            countryCodeToRegionCodeMap[808] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("KP")
            countryCodeToRegionCodeMap[850] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("HK")
            countryCodeToRegionCodeMap[852] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MO")
            countryCodeToRegionCodeMap[853] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("KH")
            countryCodeToRegionCodeMap[855] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("LA")
            countryCodeToRegionCodeMap[856] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("001")
            countryCodeToRegionCodeMap[870] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("001")
            countryCodeToRegionCodeMap[878] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BD")
            countryCodeToRegionCodeMap[880] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("001")
            countryCodeToRegionCodeMap[881] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("001")
            countryCodeToRegionCodeMap[882] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("001")
            countryCodeToRegionCodeMap[883] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TW")
            countryCodeToRegionCodeMap[886] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("001")
            countryCodeToRegionCodeMap[888] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MV")
            countryCodeToRegionCodeMap[960] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("LB")
            countryCodeToRegionCodeMap[961] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("JO")
            countryCodeToRegionCodeMap[962] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SY")
            countryCodeToRegionCodeMap[963] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("IQ")
            countryCodeToRegionCodeMap[964] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("KW")
            countryCodeToRegionCodeMap[965] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("SA")
            countryCodeToRegionCodeMap[966] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("YE")
            countryCodeToRegionCodeMap[967] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("OM")
            countryCodeToRegionCodeMap[968] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("PS")
            countryCodeToRegionCodeMap[970] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AE")
            countryCodeToRegionCodeMap[971] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("IL")
            countryCodeToRegionCodeMap[972] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BH")
            countryCodeToRegionCodeMap[973] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("QA")
            countryCodeToRegionCodeMap[974] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("BT")
            countryCodeToRegionCodeMap[975] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("MN")
            countryCodeToRegionCodeMap[976] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("NP")
            countryCodeToRegionCodeMap[977] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("001")
            countryCodeToRegionCodeMap[979] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TJ")
            countryCodeToRegionCodeMap[992] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("TM")
            countryCodeToRegionCodeMap[993] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("AZ")
            countryCodeToRegionCodeMap[994] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("GE")
            countryCodeToRegionCodeMap[995] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("KG")
            countryCodeToRegionCodeMap[996] = listWithRegionCode

            listWithRegionCode = ArrayList(1)
            listWithRegionCode.add("UZ")
            countryCodeToRegionCodeMap[998] = listWithRegionCode

            return countryCodeToRegionCodeMap
        }


        fun getFlagDrawableResId(countryCode: String): Int {
            when (countryCode.toLowerCase()) {
                "af" //afghanistan
                -> return R.drawable.flag_afghanistan
                "al" //albania
                -> return R.drawable.flag_albania
                "dz" //algeria
                -> return R.drawable.flag_algeria
                "ad" //andorra
                -> return R.drawable.flag_andorra
                "ao" //angola
                -> return R.drawable.flag_angola
                "aq" //antarctica // custom
                -> return R.drawable.flag_antarctica
                "ar" //argentina
                -> return R.drawable.flag_argentina
                "am" //armenia
                -> return R.drawable.flag_armenia
                "aw" //aruba
                -> return R.drawable.flag_aruba
                "au" //australia
                -> return R.drawable.flag_australia
                "at" //austria
                -> return R.drawable.flag_austria
                "az" //azerbaijan
                -> return R.drawable.flag_azerbaijan
                "bh" //bahrain
                -> return R.drawable.flag_bahrain
                "bd" //bangladesh
                -> return R.drawable.flag_bangladesh
                "by" //belarus
                -> return R.drawable.flag_belarus
                "be" //belgium
                -> return R.drawable.flag_belgium
                "bz" //belize
                -> return R.drawable.flag_belize
                "bj" //benin
                -> return R.drawable.flag_benin
                "bt" //bhutan
                -> return R.drawable.flag_bhutan
                "bo" //bolivia, plurinational state of
                -> return R.drawable.flag_bolivia
                "ba" //bosnia and herzegovina
                -> return R.drawable.flag_bosnia
                "bw" //botswana
                -> return R.drawable.flag_botswana
                "br" //brazil
                -> return R.drawable.flag_brazil
                "bn" //brunei darussalam // custom
                -> return R.drawable.flag_brunei
                "bg" //bulgaria
                -> return R.drawable.flag_bulgaria
                "bf" //burkina faso
                -> return R.drawable.flag_burkina_faso
                "mm" //myanmar
                -> return R.drawable.flag_myanmar
                "bi" //burundi
                -> return R.drawable.flag_burundi
                "kh" //cambodia
                -> return R.drawable.flag_cambodia
                "cm" //cameroon
                -> return R.drawable.flag_cameroon
                "ca" //canada
                -> return R.drawable.flag_canada
                "cv" //cape verde
                -> return R.drawable.flag_cape_verde
                "cf" //central african republic
                -> return R.drawable.flag_central_african_republic
                "td" //chad
                -> return R.drawable.flag_chad
                "cl" //chile
                -> return R.drawable.flag_chile
                "cn" //china
                -> return R.drawable.flag_china
                "cx" //christmas island
                -> return R.drawable.flag_christmas_island
                "cc" //cocos (keeling) islands
                -> return R.drawable.flag_cocos// custom
                "co" //colombia
                -> return R.drawable.flag_colombia
                "km" //comoros
                -> return R.drawable.flag_comoros
                "cg" //congo
                -> return R.drawable.flag_republic_of_the_congo
                "cd" //congo, the democratic republic of the
                -> return R.drawable.flag_democratic_republic_of_the_congo
                "ck" //cook islands
                -> return R.drawable.flag_cook_islands
                "cr" //costa rica
                -> return R.drawable.flag_costa_rica
                "hr" //croatia
                -> return R.drawable.flag_croatia
                "cu" //cuba
                -> return R.drawable.flag_cuba
                "cy" //cyprus
                -> return R.drawable.flag_cyprus
                "cz" //czech republic
                -> return R.drawable.flag_czech_republic
                "dk" //denmark
                -> return R.drawable.flag_denmark
                "dj" //djibouti
                -> return R.drawable.flag_djibouti
                "tl" //timor-leste
                -> return R.drawable.flag_timor_leste
                "ec" //ecuador
                -> return R.drawable.flag_ecuador
                "eg" //egypt
                -> return R.drawable.flag_egypt
                "sv" //el salvador
                -> return R.drawable.flag_el_salvador
                "gq" //equatorial guinea
                -> return R.drawable.flag_equatorial_guinea
                "er" //eritrea
                -> return R.drawable.flag_eritrea
                "ee" //estonia
                -> return R.drawable.flag_estonia
                "et" //ethiopia
                -> return R.drawable.flag_ethiopia
                "fk" //falkland islands (malvinas)
                -> return R.drawable.flag_falkland_islands
                "fo" //faroe islands
                -> return R.drawable.flag_faroe_islands
                "fj" //fiji
                -> return R.drawable.flag_fiji
                "fi" //finland
                -> return R.drawable.flag_finland
                "fr" //france
                -> return R.drawable.flag_france
                "pf" //french polynesia
                -> return R.drawable.flag_french_polynesia
                "ga" //gabon
                -> return R.drawable.flag_gabon
                "gm" //gambia
                -> return R.drawable.flag_gambia
                "ge" //georgia
                -> return R.drawable.flag_georgia
                "de" //germany
                -> return R.drawable.flag_germany
                "gh" //ghana
                -> return R.drawable.flag_ghana
                "gi" //gibraltar
                -> return R.drawable.flag_gibraltar
                "gr" //greece
                -> return R.drawable.flag_greece
                "gl" //greenland
                -> return R.drawable.flag_greenland
                "gt" //guatemala
                -> return R.drawable.flag_guatemala
                "gn" //guinea
                -> return R.drawable.flag_guinea
                "gw" //guinea-bissau
                -> return R.drawable.flag_guinea_bissau
                "gy" //guyana
                -> return R.drawable.flag_guyana
                "gf" //guyane
                -> return R.drawable.flag_guyane
                "ht" //haiti
                -> return R.drawable.flag_haiti
                "hn" //honduras
                -> return R.drawable.flag_honduras
                "hk" //hong kong
                -> return R.drawable.flag_hong_kong
                "hu" //hungary
                -> return R.drawable.flag_hungary
                "in" //india
                -> return R.drawable.flag_india
                "id" //indonesia
                -> return R.drawable.flag_indonesia
                "ir" //iran, islamic republic of
                -> return R.drawable.flag_iran
                "iq" //iraq
                -> return R.drawable.flag_iraq
                "ie" //ireland
                -> return R.drawable.flag_ireland
                "im" //isle of man
                -> return R.drawable.flag_isleof_man // custom
                "il" //israel
                -> return R.drawable.flag_israel
                "it" //italy
                -> return R.drawable.flag_italy
                "ci" //côte d\'ivoire
                -> return R.drawable.flag_cote_divoire
                "jp" //japan
                -> return R.drawable.flag_japan
                "jo" //jordan
                -> return R.drawable.flag_jordan
                "kz" //kazakhstan
                -> return R.drawable.flag_kazakhstan
                "ke" //kenya
                -> return R.drawable.flag_kenya
                "ki" //kiribati
                -> return R.drawable.flag_kiribati
                "kw" //kuwait
                -> return R.drawable.flag_kuwait
                "kg" //kyrgyzstan
                -> return R.drawable.flag_kyrgyzstan
                "ky" // Cayman Islands
                -> return R.drawable.flag_cayman_islands
                "la" //lao people\'s democratic republic
                -> return R.drawable.flag_laos
                "lv" //latvia
                -> return R.drawable.flag_latvia
                "lb" //lebanon
                -> return R.drawable.flag_lebanon
                "ls" //lesotho
                -> return R.drawable.flag_lesotho
                "lr" //liberia
                -> return R.drawable.flag_liberia
                "ly" //libya
                -> return R.drawable.flag_libya
                "li" //liechtenstein
                -> return R.drawable.flag_liechtenstein
                "lt" //lithuania
                -> return R.drawable.flag_lithuania
                "lu" //luxembourg
                -> return R.drawable.flag_luxembourg
                "mo" //macao
                -> return R.drawable.flag_macao
                "mk" //macedonia, the former yugoslav republic of
                -> return R.drawable.flag_macedonia
                "mg" //madagascar
                -> return R.drawable.flag_madagascar
                "mw" //malawi
                -> return R.drawable.flag_malawi
                "my" //malaysia
                -> return R.drawable.flag_malaysia
                "mv" //maldives
                -> return R.drawable.flag_maldives
                "ml" //mali
                -> return R.drawable.flag_mali
                "mt" //malta
                -> return R.drawable.flag_malta
                "mh" //marshall islands
                -> return R.drawable.flag_marshall_islands
                "mr" //mauritania
                -> return R.drawable.flag_mauritania
                "mu" //mauritius
                -> return R.drawable.flag_mauritius
                "yt" //mayotte
                -> return R.drawable.flag_martinique // no exact flag found
                "re" //la reunion
                -> return R.drawable.flag_martinique // no exact flag found
                "mq" //martinique
                -> return R.drawable.flag_martinique
                "mx" //mexico
                -> return R.drawable.flag_mexico
                "fm" //micronesia, federated states of
                -> return R.drawable.flag_micronesia
                "md" //moldova, republic of
                -> return R.drawable.flag_moldova
                "mc" //monaco
                -> return R.drawable.flag_monaco
                "mn" //mongolia
                -> return R.drawable.flag_mongolia
                "me" //montenegro
                -> return R.drawable.flag_of_montenegro// custom
                "ma" //morocco
                -> return R.drawable.flag_morocco
                "mz" //mozambique
                -> return R.drawable.flag_mozambique
                "na" //namibia
                -> return R.drawable.flag_namibia
                "nr" //nauru
                -> return R.drawable.flag_nauru
                "np" //nepal
                -> return R.drawable.flag_nepal
                "nl" //netherlands
                -> return R.drawable.flag_netherlands
                "nc" //new caledonia
                -> return R.drawable.flag_new_caledonia// custom
                "nz" //new zealand
                -> return R.drawable.flag_new_zealand
                "ni" //nicaragua
                -> return R.drawable.flag_nicaragua
                "ne" //niger
                -> return R.drawable.flag_niger
                "ng" //nigeria
                -> return R.drawable.flag_nigeria
                "nu" //niue
                -> return R.drawable.flag_niue
                "kp" //north korea
                -> return R.drawable.flag_north_korea
                "no" //norway
                -> return R.drawable.flag_norway
                "om" //oman
                -> return R.drawable.flag_oman
                "pk" //pakistan
                -> return R.drawable.flag_pakistan
                "pw" //palau
                -> return R.drawable.flag_palau
                "pa" //panama
                -> return R.drawable.flag_panama
                "pg" //papua new guinea
                -> return R.drawable.flag_papua_new_guinea
                "py" //paraguay
                -> return R.drawable.flag_paraguay
                "pe" //peru
                -> return R.drawable.flag_peru
                "ph" //philippines
                -> return R.drawable.flag_philippines
                "pn" //pitcairn
                -> return R.drawable.flag_pitcairn_islands
                "pl" //poland
                -> return R.drawable.flag_poland
                "pt" //portugal
                -> return R.drawable.flag_portugal
                "pr" //puerto rico
                -> return R.drawable.flag_puerto_rico
                "qa" //qatar
                -> return R.drawable.flag_qatar
                "ro" //romania
                -> return R.drawable.flag_romania
                "ru" //russian federation
                -> return R.drawable.flag_russian_federation
                "rw" //rwanda
                -> return R.drawable.flag_rwanda
                "bl" //saint barthélemy
                -> return R.drawable.flag_saint_barthelemy// custom
                "ws" //samoa
                -> return R.drawable.flag_samoa
                "sm" //san marino
                -> return R.drawable.flag_san_marino
                "st" //sao tome and principe
                -> return R.drawable.flag_sao_tome_and_principe
                "sa" //saudi arabia
                -> return R.drawable.flag_saudi_arabia
                "sn" //senegal
                -> return R.drawable.flag_senegal
                "rs" //serbia
                -> return R.drawable.flag_serbia // custom
                "sc" //seychelles
                -> return R.drawable.flag_seychelles
                "sl" //sierra leone
                -> return R.drawable.flag_sierra_leone
                "sg" //singapore
                -> return R.drawable.flag_singapore
                "sx" // Sint Maarten
                ->
                    //TODO: Add Flag.
                    return 0
                "sk" //slovakia
                -> return R.drawable.flag_slovakia
                "si" //slovenia
                -> return R.drawable.flag_slovenia
                "sb" //solomon islands
                -> return R.drawable.flag_soloman_islands
                "so" //somalia
                -> return R.drawable.flag_somalia
                "za" //south africa
                -> return R.drawable.flag_south_africa
                "kr" //south korea
                -> return R.drawable.flag_south_korea
                "es" //spain
                -> return R.drawable.flag_spain
                "lk" //sri lanka
                -> return R.drawable.flag_sri_lanka
                "sh" //saint helena, ascension and tristan da cunha
                -> return R.drawable.flag_saint_helena // custom
                "pm" //saint pierre and miquelon
                -> return R.drawable.flag_saint_pierre
                "sd" //sudan
                -> return R.drawable.flag_sudan
                "sr" //suriname
                -> return R.drawable.flag_suriname
                "sz" //swaziland
                -> return R.drawable.flag_swaziland
                "se" //sweden
                -> return R.drawable.flag_sweden
                "ch" //switzerland
                -> return R.drawable.flag_switzerland
                "sy" //syrian arab republic
                -> return R.drawable.flag_syria
                "tw" //taiwan, province of china
                -> return R.drawable.flag_taiwan
                "tj" //tajikistan
                -> return R.drawable.flag_tajikistan
                "tz" //tanzania, united republic of
                -> return R.drawable.flag_tanzania
                "th" //thailand
                -> return R.drawable.flag_thailand
                "tg" //togo
                -> return R.drawable.flag_togo
                "tk" //tokelau
                -> return R.drawable.flag_tokelau // custom
                "to" //tonga
                -> return R.drawable.flag_tonga
                "tn" //tunisia
                -> return R.drawable.flag_tunisia
                "tr" //turkey
                -> return R.drawable.flag_turkey
                "tm" //turkmenistan
                -> return R.drawable.flag_turkmenistan
                "tv" //tuvalu
                -> return R.drawable.flag_tuvalu
                "ae" //united arab emirates
                -> return R.drawable.flag_uae
                "ug" //uganda
                -> return R.drawable.flag_uganda
                "gb" //united kingdom
                -> return R.drawable.flag_united_kingdom
                "ua" //ukraine
                -> return R.drawable.flag_ukraine
                "uy" //uruguay
                -> return R.drawable.flag_uruguay
                "us" //united states
                -> return R.drawable.flag_united_states_of_america
                "uz" //uzbekistan
                -> return R.drawable.flag_uzbekistan
                "vu" //vanuatu
                -> return R.drawable.flag_vanuatu
                "va" //holy see (vatican city state)
                -> return R.drawable.flag_vatican_city
                "ve" //venezuela, bolivarian republic of
                -> return R.drawable.flag_venezuela
                "vn" //vietnam
                -> return R.drawable.flag_vietnam
                "wf" //wallis and futuna
                -> return R.drawable.flag_wallis_and_futuna
                "ye" //yemen
                -> return R.drawable.flag_yemen
                "zm" //zambia
                -> return R.drawable.flag_zambia
                "zw" //zimbabwe
                -> return R.drawable.flag_zimbabwe

            // Caribbean Islands
                "ai" //anguilla
                -> return R.drawable.flag_anguilla
                "ag" //antigua & barbuda
                -> return R.drawable.flag_antigua_and_barbuda
                "bs" //bahamas
                -> return R.drawable.flag_bahamas
                "bb" //barbados
                -> return R.drawable.flag_barbados
                "bm" //bermuda
                -> return R.drawable.flag_bermuda
                "vg" //british virgin islands
                -> return R.drawable.flag_british_virgin_islands
                "dm" //dominica
                -> return R.drawable.flag_dominica
                "do" //dominican republic
                -> return R.drawable.flag_dominican_republic
                "gd" //grenada
                -> return R.drawable.flag_grenada
                "jm" //jamaica
                -> return R.drawable.flag_jamaica
                "ms" //montserrat
                -> return R.drawable.flag_montserrat
                "kn" //st kitts & nevis
                -> return R.drawable.flag_saint_kitts_and_nevis
                "lc" //st lucia
                -> return R.drawable.flag_saint_lucia
                "vc" //st vincent & the grenadines
                -> return R.drawable.flag_saint_vicent_and_the_grenadines
                "tt" //trinidad & tobago
                -> return R.drawable.flag_trinidad_and_tobago
                "tc" //turks & caicos islands
                -> return R.drawable.flag_turks_and_caicos_islands
                "vi" //us virgin islands
                -> return R.drawable.flag_us_virgin_islands
                else -> return 0
            }
        }
    }
}