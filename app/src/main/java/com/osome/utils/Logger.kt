package com.osome.utils

import android.content.Context
import android.util.Log
import android.widget.Toast
import com.osome.BuildConfig

class Logger {
    companion object {

        fun log(text: String) {
            if (BuildConfig.BUILD_TYPE.contentEquals("stage") || BuildConfig.BUILD_TYPE.contentEquals("debug"))
                Log.d("SSSSSSSSSSSS", text)
        }

        fun errorToast(context: Context, text: String) {
            if (BuildConfig.BUILD_TYPE.contentEquals("stage") || BuildConfig.BUILD_TYPE.contentEquals("debug"))
                Toast.makeText(context, text, Toast.LENGTH_LONG).show()
        }
    }
}