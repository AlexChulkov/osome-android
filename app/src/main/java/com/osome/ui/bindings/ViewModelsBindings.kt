package com.osome.ui.bindings

import android.databinding.DataBindingComponent


class ViewModelsBindings : DataBindingComponent {

    override fun getBindings(): Bindings {
        return Bindings()
    }

    override fun getEditTextBinding(): EditTextBinding {
        return EditTextBinding()
    }
}