package com.osome.ui.bindings

import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.databinding.InverseBindingListener
import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

class EditTextBinding {

    @BindingAdapter("text")
    fun setText(view: EditText, value: String) {
        if (view.text.toString() != value)
            view.setText(value)
    }

    @InverseBindingAdapter(attribute = "text")
    fun getText(view: EditText): String {
        return view.text.toString()
    }


    @BindingAdapter("textAttrChanged")
    fun setListener(view: EditText, listener: InverseBindingListener?) {
        if (listener != null) {
            view.addTextChangedListener(object : TextWatcher {

                override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

                override fun afterTextChanged(editable: Editable) {
                    listener.onChange()
                }
            })
        }
    }
}
