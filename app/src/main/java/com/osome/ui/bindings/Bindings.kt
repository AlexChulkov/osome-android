package com.osome.ui.bindings

import android.databinding.BindingAdapter
import android.support.v7.widget.RecyclerView
import android.text.TextWatcher
import android.widget.EditText
import android.widget.ImageView
import com.osome.ui.views.adapters.TabbedPagerAdapter
import com.osome.ui.views.centerlockviewpager.TabLayout
import com.osome.ui.views.centerlockviewpager.ViewPager

class Bindings {

    @BindingAdapter("bind:listAdapter")
    fun setRecyclerViewAdapter(recyclerView: RecyclerView, adapter: RecyclerView.Adapter<RecyclerView.ViewHolder>) {
        recyclerView.adapter = adapter
    }

    @BindingAdapter("bind:layoutManager")
    fun setRecyclerViewAdapter(recyclerView: RecyclerView, layoutManager: RecyclerView.LayoutManager ) {
        recyclerView.layoutManager = layoutManager
    }

//    @BindingAdapter("bind:scrollListener")
//    fun setRecyclerViewAdapter(recyclerView: RecyclerView, scrollListener: RecyclerView.OnScrollListener) {
//        recyclerView.addOnScrollListener(scrollListener)
//    }
//
//
//    @BindingAdapter("bind:refreshCallback")
//    fun setRefreshCallback(swipeRefreshLayout: SwipeRefreshLayout, listener: SwipeRefreshLayout.OnRefreshListener) {
//        swipeRefreshLayout.setOnRefreshListener({
//            Observable.timer(500, TimeUnit.MILLISECONDS).observeOn(AndroidSchedulers.mainThread()).subscribe {
//                swipeRefreshLayout.isRefreshing = false
//            }
//            listener.onRefresh()
//        })
//    }


    @BindingAdapter("android:src")
    fun setImageViewResource(imageView: ImageView, resource: Int) {
        imageView.setImageResource(resource)
    }

    @BindingAdapter("bind:textWatcher")
    fun setImageViewResource(editText: EditText, textWatcher: TextWatcher) {
        editText.addTextChangedListener(textWatcher)
    }

    @BindingAdapter("bind:viewPagerAdapter", "bind:tabs")
    fun setViewPagerAdapter(viewPager: ViewPager, viewPagerAdapter: TabbedPagerAdapter, tabs: TabLayout) {
        viewPager.offscreenPageLimit = viewPagerAdapter.getOffscreenPageLimit()
        viewPager.setAdapter(viewPagerAdapter)
        viewPager.setCenterItem(viewPagerAdapter.getStartPage())

        tabs.setupWithViewPager(viewPager)
        for (i in 0 until tabs.tabCount) {
            val tab = tabs.getTabAt(i)
            tab!!.customView = viewPagerAdapter.getTabView(i)
        }
    }
}