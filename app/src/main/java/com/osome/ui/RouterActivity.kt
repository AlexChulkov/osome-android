package com.osome.ui

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import com.osome.App
import com.osome.R
import com.osome.data.DataManager
import com.osome.ui.auth.AuthFragment
import com.osome.ui.mainscreen.MainScreenFragment
import com.osome.ui.test.TestFragment
import java.util.*
import javax.inject.Inject

class RouterActivity : AppCompatActivity() {


    @Inject lateinit var dataManager: DataManager

    val fragments = HashMap<Int, BaseViewModelFragment>()
    val FRAGMENT_TAG = "fragment_tag"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
//            window.statusBadfgrColor = Color.WHITE
//        }
        (applicationContext as App).osomeComponent.inject(this)
        var fragment : BaseViewModelFragment? = supportFragmentManager.findFragmentByTag(FRAGMENT_TAG) as BaseViewModelFragment?
        if (fragment==null) {
            fragment = getStartFragment()
//            fragment = TestFragment()
        }
        val transaction = supportFragmentManager.beginTransaction()
        transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
        transaction.replace(R.id.fragment, fragment, FRAGMENT_TAG)
        transaction.commit()
    }

    private fun getStartFragment(): BaseViewModelFragment? {
        return when (dataManager.authState) {
            DataManager.AuthState.STATE_FIRST_AUTH -> AuthFragment()
            DataManager.AuthState.STATE_REGISTERED -> MainScreenFragment()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            android.R.id.home -> onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }
}
