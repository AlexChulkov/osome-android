package com.osome.ui.leftmenu

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.osome.R
import com.osome.databinding.FragmentLeftMenuBinding
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment

class LeftMenuFragment :  BaseViewModelFragment() {

    lateinit var binding: FragmentLeftMenuBinding
    var leftMenuFragmentViewModel: LeftMenuFragmentViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_left_menu, container, false)
        return binding.root
    }

    override fun createViewModelAndBind(): BaseViewModel {
        if (leftMenuFragmentViewModel == null) {
            leftMenuFragmentViewModel = LeftMenuFragmentViewModel(this)
        }
        binding.viewModel = leftMenuFragmentViewModel
        return leftMenuFragmentViewModel!!
    }

}