package com.osome.ui

import android.databinding.BindingAdapter
import android.databinding.InverseBindingAdapter
import android.os.Bundle
import android.support.v4.app.Fragment
import android.widget.EditText
import rx.Subscription

abstract class BaseViewModelFragment : Fragment() {

    lateinit var viewModel : BaseViewModel
    protected val subs = mutableListOf<Subscription>()
    abstract fun createViewModelAndBind(): BaseViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel =  createViewModelAndBind()
    }

    override fun onStart() {
        super.onStart()
        ac().fragments.put(System.identityHashCode(this), this)
        viewModel.onStart()
    }

    override fun onPause() {
        super.onPause()
        viewModel.onPause()
        subs.forEach { it.unsubscribe() }
        subs.clear()
    }

    fun onBackPressed(): Boolean {
        if (ac().supportFragmentManager.backStackEntryCount != 0) {
            ac().supportFragmentManager.popBackStack()
            return true
        } else {
            return false
        }
    }

    fun ac() = activity as RouterActivity
}