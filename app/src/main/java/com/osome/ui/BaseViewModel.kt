package com.osome.ui

import android.content.Context
import com.osome.App
import com.osome.data.DataManager
import com.osome.net.HttpClient
import rx.Subscription
import javax.inject.Inject

abstract class BaseViewModel (context: Context) {

    @Inject protected lateinit var dataManager: DataManager
    @Inject protected lateinit var httpClient: HttpClient
    @Inject protected lateinit var context: Context

    protected val subs = mutableListOf<Subscription>()

    init {
        (context.applicationContext as App).osomeComponent.inject(this)
    }

    open fun onStart() {

    }

    open fun onPause() {
        subs.forEach { it.unsubscribe() }
        subs.clear()
    }
}