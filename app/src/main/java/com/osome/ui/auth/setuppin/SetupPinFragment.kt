package com.osome.ui.auth.setuppin

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.osome.R
import com.osome.databinding.FragmentSetupPinBinding
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment
import com.osome.ui.mainscreen.MainScreenFragment
import com.osome.utils.SystemUtils

class SetupPinFragment : BaseViewModelFragment() {

    lateinit var binding : FragmentSetupPinBinding
    var setupPinViewModel: SetupPinFragmentViewModel? = null

    companion object {
        val VERIFICATION_TOKEN = "VERIFICATION_TOKEN"
        fun createInstance(verificationToken: String): SetupPinFragment {
            val fragment = SetupPinFragment()
            val bundle = Bundle()
            bundle.putString(VERIFICATION_TOKEN, verificationToken)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_setup_pin, container, false)
        return binding.root
    }

    override fun createViewModelAndBind(): BaseViewModel {
        if (setupPinViewModel==null) {
            setupPinViewModel = SetupPinFragmentViewModel(this, arguments!!.getString(VERIFICATION_TOKEN))
        }
        binding.viewModel = setupPinViewModel
        return setupPinViewModel!!
    }

    override fun onStart() {
        super.onStart()
        subs += setupPinViewModel?.needShowNextScreen!!.subscribe {
            SystemUtils.hideKeyboard(binding.pinView, ac())
            ac().supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            val fragment = MainScreenFragment()
            val transaction = ac().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
            transaction.replace(R.id.fragment, fragment, ac().FRAGMENT_TAG).commit()
        }
    }
}