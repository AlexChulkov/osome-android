package com.osome.ui.auth

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.osome.R
import com.osome.databinding.FragmentAuthBinding
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment
import com.osome.ui.auth.verification.SmsVerificationFragment
import com.osome.ui.selectcountry.SelectCountryFragment
import com.osome.utils.SystemUtils

class AuthFragment : BaseViewModelFragment() {

    lateinit var binding: FragmentAuthBinding
    var authFragmentViewModel: AuthFragmentViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_auth, container, false)
        return binding.root
    }

    override fun createViewModelAndBind(): BaseViewModel {
        if (authFragmentViewModel == null) {
            authFragmentViewModel = AuthFragmentViewModel(this)
        }
        binding.viewModel = authFragmentViewModel
        return authFragmentViewModel!!
    }

    override fun onResume() {
        super.onResume()
        subs += authFragmentViewModel?.needShowNextScreen!!.subscribe {
            SystemUtils.hideKeyboard(binding.phoneNumber, ac())
            val fragment = SmsVerificationFragment.createInstance(it)
            val transaction = ac().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
            transaction.addToBackStack("")
            transaction.replace(R.id.fragment, fragment, ac().FRAGMENT_TAG).commit()
        }
        subs += authFragmentViewModel?.needShowCountrySelector!!.distinctUntilChanged().subscribe {
            SystemUtils.hideKeyboard(binding.phoneNumber, ac())
            val fragment = SelectCountryFragment()
            fragment.countrySelectedCallback = countrySelectedCallback
            val transaction = ac().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
            transaction.addToBackStack(null)
            transaction.replace(R.id.fragment, fragment, ac().FRAGMENT_TAG).commit()
        }
    }

    val countrySelectedCallback = { country: String ->
        authFragmentViewModel!!.selectedCountry = country
    }
}