package com.osome.ui.auth.setuppin

import android.databinding.Observable
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.widget.Toast
import com.osome.R
import com.osome.data.DataManager
import com.osome.data.User
import com.osome.net.ErrorEntity
import com.osome.net.HandlerCallback
import com.osome.net.responses.BaseResponse
import com.osome.net.responses.SetupPinResponse
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment
import com.osome.utils.SharedPreferencesUtils
import rx.subjects.BehaviorSubject

class SetupPinFragmentViewModel(fragment: BaseViewModelFragment, val confirmationToken: String): BaseViewModel(fragment.ac()) {

    val textColor = context.resources.getColor(R.color.black_color)
    val errorColor = context.resources.getColor(R.color.error_text_color)

    val hint = context.getString(R.string.setup_pin_text)
    val errorText = context.getString(R.string.pins_doesnt_match)


    val hintText = ObservableField<String>(hint)
    val hintColor = ObservableField<Int>(textColor)
    val pinText = ObservableField<String>()
    val requestInProgress = ObservableBoolean(false)
    val needShowNextScreen = BehaviorSubject.create<Void>()
    val state = BehaviorSubject.create(SetupState.FirstField)
    val VALIDATION_CODE_LENGHT = 4

    enum class SetupState {
        FirstField,
        WrongTyped;
    }

    init {
        pinText.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (pinText.get()!!.length == VALIDATION_CODE_LENGHT) {
                    checkValidationCode()
                } else if (pinText.get()!!.length == 1) {
                    state.onNext(SetupState.FirstField)
                }
            }
        })
    }

    private fun checkValidationCode() {
        httpClient.setupPin(context, confirmationToken, pinText.get()!!, object : HandlerCallback<BaseResponse<SetupPinResponse>>() {
            override fun onError(error: ErrorEntity) {
                Toast.makeText(context, " " + error.errorCode + " " + error.errorDesc, Toast.LENGTH_LONG).show()
                requestInProgress.set(false)
                cleanPinAndShowErrorToast()
            }

            override fun onComplite(result: BaseResponse<SetupPinResponse>) {
                requestInProgress.set(false)
                if (result.success) {
                    dataManager.authState = DataManager.AuthState.STATE_REGISTERED
                    dataManager.user = result.data!!.user
                    dataManager.accessToken = result.data!!.token
                    needShowNextScreen.onNext(null)
                } else {
                    cleanPinAndShowErrorToast()
                }
            }
        })
    }

    override fun onStart() {
        super.onStart()
        subs+= state.distinctUntilChanged().subscribe{
            when(it) {
                SetupState.FirstField -> {
                    hintText.set(hint)
                    hintColor.set(textColor)
                }
                SetupState.WrongTyped -> {
                    hintText.set(errorText)
                    hintColor.set(errorColor)
                }
            }
        }
    }

    private fun cleanPinAndShowErrorToast() {
        pinText.set("")
        state.onNext(SetupState.WrongTyped)
    }
}