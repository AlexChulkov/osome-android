package com.osome.ui.auth.verification

import android.databinding.Observable
import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.widget.Toast
import com.osome.R
import com.osome.data.DataManager
import com.osome.net.ErrorEntity
import com.osome.net.HandlerCallback
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment
import okhttp3.ResponseBody
import rx.subjects.BehaviorSubject

class SmsVerificationFragmentViewModel(fragment: BaseViewModelFragment, val confirmationToken: String): BaseViewModel(fragment.ac()) {
    val textColor = context.resources.getColor(R.color.black_color)
    val errorColor = context.resources.getColor(R.color.error_text_color)

    val hint = context.getString(R.string.validation_code_hint)
    val errorText = context.getString(R.string.wrong_validation_code)


    val hintText = ObservableField<String>(hint)
    val hintColor = ObservableField<Int>(textColor)
    val validationText = ObservableField<String>()
    val requestInProgress = ObservableBoolean(false)
    val needShowNextScreen = BehaviorSubject.create<Void>()
    val VALIDATION_CODE_LENGHT = 4

    init {
        validationText.addOnPropertyChangedCallback(object : Observable.OnPropertyChangedCallback() {
            override fun onPropertyChanged(sender: Observable?, propertyId: Int) {
                if (validationText.get()!!.length == VALIDATION_CODE_LENGHT) {
                    checkValidationCode()
                } else if (validationText.get()!!.length == 1) {
                    hintText.set(hint)
                    hintColor.set(textColor)
                }
            }
        })
    }

    private fun checkValidationCode() {
        requestInProgress.set(true)
        httpClient.validateSmsCode(context, confirmationToken, validationText.get()!!, object : HandlerCallback<ResponseBody>() {
            override fun onError(error: ErrorEntity) {
                Toast.makeText(context, " " + error.errorCode + " " + error.errorDesc, Toast.LENGTH_LONG).show()
                requestInProgress.set(false)
                cleanPinAndShowerrorToast()
            }

            override fun onComplite(result: ResponseBody) {
                requestInProgress.set(false)
                needShowNextScreen.onNext(null)
            }
        })
    }

    private fun cleanPinAndShowerrorToast() {
        validationText.set("")
        hintText.set(errorText)
        hintColor.set(errorColor)
    }


}