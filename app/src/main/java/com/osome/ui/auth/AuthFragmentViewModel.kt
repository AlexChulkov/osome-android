package com.osome.ui.auth

import android.databinding.ObservableBoolean
import android.databinding.ObservableField
import android.databinding.ObservableInt
import android.os.Build
import android.telephony.PhoneNumberFormattingTextWatcher
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.osome.R
import com.osome.net.ErrorEntity
import com.osome.net.HandlerCallback
import com.osome.net.responses.BaseResponse
import com.osome.net.responses.RequestSmsResponse
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment
import com.osome.utils.CountryCodesUtil
import com.osome.utils.Logger
import rx.Observable
import rx.subjects.BehaviorSubject
import rx.subjects.PublishSubject


class AuthFragmentViewModel(fragment: BaseViewModelFragment): BaseViewModel(fragment.ac()) {

    var phoneNumber = ObservableField<String>("9959036782")
    var phoneCode = ObservableField<String>()
    var flagImageRes = ObservableInt()
    var phoneFormatter = ObservableField<PhoneNumberFormattingTextWatcher>()
    var phoneRequested = ObservableBoolean(false)
    var needShowNextScreen = PublishSubject.create<String>()
    var needShowCountrySelector = PublishSubject.create<Void>()
//    var selectedCountry: String = getDefaultLocale()
    var selectedCountry: String = "RU"

    private fun getDefaultLocale(): String {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return context.resources.configuration.locales.get(0).country
        } else {
            return context.resources.configuration.locale.country
        }
    }

    override fun onStart() {
        super.onStart()
        if (!TextUtils.isEmpty(selectedCountry)) {
            flagImageRes.set(CountryCodesUtil.getFlagDrawableResId(selectedCountry))
            CountryCodesUtil.getCountryCodeToRegionCodeMap().forEach { if (it.value.contains(selectedCountry))
                phoneCode.set("+" + it.key)
            }
            phoneFormatter.set(PhoneNumberFormattingTextWatcher(selectedCountry))
        }
    }


    fun openCountrySelector(view: View) {
        needShowCountrySelector.onNext(null)
    }

    fun requestSmsButtonClick(view: View) {
        phoneNumber.set(getValidPhoneNumber(phoneNumber.get()!!))
        val phoneNumber = phoneNumber.get()!!
        if (!TextUtils.isEmpty(phoneNumber)) {
            phoneRequested.set(true)
            httpClient.requestSms(context, phoneCode.get() + getValidPhoneNumber(phoneNumber), object : HandlerCallback<BaseResponse<RequestSmsResponse>>() {
                override fun onComplite(result: BaseResponse<RequestSmsResponse>) {
                    phoneRequested.set(false)
                    if (result.data!=null && !TextUtils.isEmpty(result.data!!.confirmationToken)) {
                        needShowNextScreen.onNext(result.data!!.confirmationToken)
                    } else {
                        Logger.errorToast(context, context.getString(R.string.empty_confirmation_token))
                    }
                }

                override fun onError(error: ErrorEntity) {
                    Logger.errorToast(context, " " + error.errorCode + " " + error.errorDesc)
                    phoneRequested.set(false)
                }
            })
        }
    }

    private fun getValidPhoneNumber(phoneNumber: String): String {
        return phoneNumber.replace("[^\\d.]".toRegex(), "")
    }
}