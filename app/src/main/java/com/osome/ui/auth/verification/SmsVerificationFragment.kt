package com.osome.ui.auth.verification

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.app.FragmentManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.osome.R
import com.osome.databinding.FragmentSmsVerificationBinding
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment
import com.osome.ui.auth.setuppin.SetupPinFragment
import com.osome.ui.mainscreen.MainScreenFragment
import com.osome.utils.SystemUtils

class SmsVerificationFragment: BaseViewModelFragment() {

    lateinit var binding: FragmentSmsVerificationBinding
    var smsVerificationFragmentViewModel: SmsVerificationFragmentViewModel? = null

    companion object {
        val VERIFICATION_TOKEN = "VERIFICATION_TOKEN"
        fun createInstance(verificationToken: String): SmsVerificationFragment {
            val fragment = SmsVerificationFragment()
            val bundle = Bundle()
            bundle.putString(VERIFICATION_TOKEN, verificationToken)
            fragment.arguments = bundle
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_sms_verification, container, false)
        return binding.root
    }

    override fun createViewModelAndBind(): BaseViewModel {
        if (smsVerificationFragmentViewModel==null) {
            smsVerificationFragmentViewModel = SmsVerificationFragmentViewModel(this, arguments!!.getString(VERIFICATION_TOKEN))
        }
        binding.viewModel = smsVerificationFragmentViewModel
        return smsVerificationFragmentViewModel!!
    }


    override fun onStart() {
        super.onStart()
        subs += smsVerificationFragmentViewModel?.needShowNextScreen!!.subscribe {
            SystemUtils.hideKeyboard(binding.pinView, ac())
            ac().supportFragmentManager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
            val fragment = SetupPinFragment.createInstance(arguments!!.getString(VERIFICATION_TOKEN))
            val transaction = ac().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
            transaction.replace(R.id.fragment, fragment, ac().FRAGMENT_TAG).commit()
        }
    }
}