package com.osome.ui.test

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.osome.R
import com.osome.databinding.FragmentTestBinding
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment
import com.osome.ui.mainscreen.MainScreenFragment

class Test2Fragment : BaseViewModelFragment() {

    lateinit var binding: FragmentTestBinding
    var testFragmentViewModel: TestFragmentViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_2test, container, false)
        return binding.root
    }

    override fun createViewModelAndBind(): BaseViewModel {
        if (testFragmentViewModel == null) {
            testFragmentViewModel = TestFragmentViewModel(this.ac())
        }
        binding.viewModel = testFragmentViewModel
        return testFragmentViewModel!!
    }

    override fun onStart() {
        super.onStart()
        subs += testFragmentViewModel?.needShowNextScreen!!.subscribe {
            val fragment = MainScreenFragment()
            val transaction = ac().supportFragmentManager.beginTransaction()
            transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, android.R.anim.fade_in, android.R.anim.fade_out)
            transaction.addToBackStack(null).replace(R.id.fragment, fragment, ac().FRAGMENT_TAG).commit()
        }
    }
}