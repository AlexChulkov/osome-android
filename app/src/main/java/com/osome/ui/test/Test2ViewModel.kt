package com.osome.ui.test

import android.content.Context
import android.view.View
import com.osome.ui.BaseViewModel
import rx.subjects.PublishSubject

class Test2ViewModel (context: Context): BaseViewModel(context) {
    var needShowNextScreen = PublishSubject.create<Void>()


    fun onClick(view: View) {
        needShowNextScreen.onNext(null)
    }
}