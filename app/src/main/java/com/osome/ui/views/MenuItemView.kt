package com.osome.ui.views

import android.content.Context
import android.text.TextUtils
import android.util.AttributeSet
import android.widget.LinearLayout
import android.widget.TextView
import android.view.LayoutInflater
import android.view.Gravity
import android.view.View
import android.widget.ImageView
import com.osome.R


class MenuItemView : LinearLayout {

    private var mImage: ImageView
    private var mTextView: TextView
    private var divider: View

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val a = context.obtainStyledAttributes(attrs, R.styleable.MenuItemView, 0, 0)
        val titleText = a.getString(R.styleable.MenuItemView_titleText)
        val image = a.getDrawable(R.styleable.MenuItemView_imageIcon)
        val showDivider = a.getBoolean(R.styleable.MenuItemView_showDivider, true)
        a.recycle()

        orientation = LinearLayout.VERTICAL
        gravity = Gravity.CENTER_VERTICAL
        background = context.getDrawable(R.drawable.transparent_button_bg_white)

        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        inflater.inflate(R.layout.view_menu_item, this, true)

        mImage = findViewById(R.id.image)
        mTextView = findViewById(R.id.text)
        divider = findViewById(R.id.divider)
        if (!TextUtils.isEmpty(titleText))
            mTextView.text = titleText
        image?.let { mImage.setImageDrawable(image) }
        divider.visibility = if (showDivider) View.VISIBLE else View.GONE
    }

}