package com.osome.ui.views.adapters

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.view.View

abstract class TabbedPagerAdapter(fm : FragmentManager) : FragmentPagerAdapter(fm) {

    abstract fun getTabView(position : Int) : View
    abstract fun getOffscreenPageLimit() : Int
    abstract fun getStartPage() : Int

}