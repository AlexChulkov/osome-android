package com.osome.ui.selectcountry

import com.osome.databinding.LayoutCountryHeaderBinding
import com.osome.databinding.LayoutCountryItemBinding
import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.ViewGroup
import com.osome.R

class SelectCountryAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val dataArray = ArrayList<SelectCountryFragmentViewModel.CountrySelectorData>()
    private val VIEW_ITEM = R.layout.layout_country_item
    private val VIEW_HEADER =  R.layout.layout_country_header

    init {
        setHasStableIds(true)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val vh: RecyclerView.ViewHolder
        if (viewType == VIEW_ITEM) {
            val binding = DataBindingUtil.inflate<LayoutCountryItemBinding>(layoutInflater, viewType, parent, false)
            vh = CountryViewHolder(binding)
        } else {
            val v = DataBindingUtil.inflate<LayoutCountryHeaderBinding>(layoutInflater, viewType, parent, false)
            vh = HeaderViewHolder(v)
        }
        return vh
    }

    fun setData(data: List<SelectCountryFragmentViewModel.CountrySelectorData>) {
        dataArray.clear()
        dataArray.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return dataArray.size
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position)== VIEW_ITEM) {
            (holder as CountryViewHolder).bind(dataArray[position])
        } else {
            (holder as HeaderViewHolder).bind(dataArray[position])
        }
    }

    override fun getItemViewType(position: Int): Int {
        if (TextUtils.isEmpty(dataArray[position].headerTitle)) {
            return VIEW_ITEM
        } else {
            return VIEW_HEADER
        }
    }

    class CountryViewHolder(private val binding: LayoutCountryItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind (data: SelectCountryFragmentViewModel.CountrySelectorData) {
            binding.data = data
        }
    }

    class HeaderViewHolder(private val binding: LayoutCountryHeaderBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind (data: SelectCountryFragmentViewModel.CountrySelectorData) {
            binding.data = data
        }
    }

}