package com.osome.ui.selectcountry

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v4.view.MenuItemCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.*
import android.widget.SearchView
import com.osome.R
import com.osome.databinding.FragmentSelectCountryBinding
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment

class SelectCountryFragment: BaseViewModelFragment() {

    lateinit var binding: FragmentSelectCountryBinding
    var selectCountryFragmentViewModel: SelectCountryFragmentViewModel? = null
    lateinit var countrySelectedCallback: (country:String) -> Unit

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_select_country, container, false)
        ac().setSupportActionBar(binding.toolbar)
        return binding.root
    }

    override fun createViewModelAndBind(): BaseViewModel {
        if (selectCountryFragmentViewModel==null) {
            selectCountryFragmentViewModel = SelectCountryFragmentViewModel(this)
        }
        selectCountryFragmentViewModel!!.layoutManager = LinearLayoutManager(context)
        binding.viewModel = selectCountryFragmentViewModel
        return selectCountryFragmentViewModel!!
    }

    override fun onStart() {
        super.onStart()
        subs += selectCountryFragmentViewModel!!.onItemClick.distinctUntilChanged().subscribe {
            countrySelectedCallback.invoke(it)
            ac().onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.menu_select_country, menu)

        val searchItem = menu!!.findItem(R.id.action_search)
        val searchView = MenuItemCompat.getActionView(searchItem) as SearchView
        searchView.setOnQueryTextListener(selectCountryFragmentViewModel)
        searchView.setOnCloseListener(selectCountryFragmentViewModel)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onResume() {
        super.onResume()
        val actionBar = (activity as AppCompatActivity).supportActionBar
        actionBar?.title = getString(R.string.title_select_country)
        actionBar?.setDisplayHomeAsUpEnabled(true)
        actionBar?.setHomeButtonEnabled(true)
    }
}