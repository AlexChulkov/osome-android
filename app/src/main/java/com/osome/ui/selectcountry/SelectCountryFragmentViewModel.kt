package com.osome.ui.selectcountry

import android.support.v7.widget.LinearLayoutManager
import android.text.TextUtils
import android.view.View
import android.widget.SearchView
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment
import com.osome.utils.CountryCodesUtil
import rx.subjects.BehaviorSubject
import java.util.*
import kotlin.collections.ArrayList

class SelectCountryFragmentViewModel(fragment: BaseViewModelFragment): BaseViewModel(fragment.ac()), SearchView.OnQueryTextListener, SearchView.OnCloseListener {

    private val countriesMap = createDataArray()
    val adapter = initAdapter()

    private fun initAdapter(): SelectCountryAdapter {
        val selectCountryAdapter = SelectCountryAdapter()
        selectCountryAdapter.setData(countriesMap)
        return selectCountryAdapter
    }

    var layoutManager: LinearLayoutManager? = null
    val onItemClick = BehaviorSubject.create<String>()


    fun createDataArray(): ArrayList<CountrySelectorData> {
        val list = CountryCodesUtil.getCountryCodeToRegionCodeMap()
        var result = arrayListOf<CountrySelectorData>()
        list.forEach { phoneCode, counriesList ->
            counriesList.forEach { country->
                val loc = Locale("", country)
                val data = CountrySelectorData()
                data.description = loc.displayCountry + "(+" + phoneCode + ")"
                data.countryCode = country
                data.flagDrawableRes = CountryCodesUtil.getFlagDrawableResId(country)
                if (!TextUtils.isEmpty(loc.displayCountry) && data.flagDrawableRes!=0) {
                    result.add(data)
                }
            }
        }
        result.sortBy {it.description}
        var oldItemStartByLetter = ""
        var i = 0
        while (i < result.size-1) {
            val startByLetter = result[i].description[0].toString()
            if (startByLetter!=oldItemStartByLetter) {
                val data = CountrySelectorData()
                data.headerTitle = startByLetter
                result.add(i, data)
            }
            i++
            oldItemStartByLetter = startByLetter
        }
        return result
    }

    inner class CountrySelectorData {
        var headerTitle: String= ""
        var countryCode: String= ""
        var description: String= ""
        var flagDrawableRes: Int = 0

        fun onClick(view: View) {
            onItemClick.onNext(countryCode)
        }
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        val filteredData = countriesMap.filter { it.description.toLowerCase().startsWith(query!!.toLowerCase()) }
        adapter.setData(filteredData)
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        val filteredData = countriesMap.filter { it.description.toLowerCase().startsWith(newText!!.toLowerCase()) }
        adapter.setData(filteredData)
        return true
    }

    override fun onClose(): Boolean {
        adapter.setData(countriesMap)
        return false
    }

}