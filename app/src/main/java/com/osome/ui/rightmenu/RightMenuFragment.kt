package com.osome.ui.rightmenu

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.osome.R
import com.osome.databinding.FragmentRightMenuBinding
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment

class RightMenuFragment: BaseViewModelFragment() {

    lateinit var binding: FragmentRightMenuBinding
    var rightFragmentViewModel: RightMenuViewModel? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        retainInstance = true
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_right_menu, container, false)
        return binding.root
    }
    override fun createViewModelAndBind(): BaseViewModel {
        if (rightFragmentViewModel==null) {
            rightFragmentViewModel = RightMenuViewModel(ac())
        }

        binding.viewModel = rightFragmentViewModel
        return rightFragmentViewModel!!
    }

}