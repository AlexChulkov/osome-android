package com.osome.ui.rightmenu

import android.content.Context
import android.databinding.ObservableInt
import android.graphics.Color
import com.osome.ui.BaseViewModel
import java.util.*

class RightMenuViewModel(context: Context): BaseViewModel (context) {

    val color = ObservableInt(getRandomColor())

    private fun getRandomColor(): Int {
        val rnd = Random()
        return Color.argb(255, rnd.nextInt(256), rnd.nextInt(256), rnd.nextInt(256))
    }
}