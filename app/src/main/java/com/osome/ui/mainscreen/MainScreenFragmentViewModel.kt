package com.osome.ui.mainscreen

import com.osome.net.ErrorEntity
import com.osome.net.HandlerCallback
import com.osome.net.responses.BaseResponse
import com.osome.net.responses.CompanyesResponse
import com.osome.net.responses.ConversationsResponse
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment
import com.osome.utils.Logger
import rx.subjects.PublishSubject

class MainScreenFragmentViewModel(fragment: BaseViewModelFragment): BaseViewModel(fragment.ac()) {
    val adapter = MainScreenPagerAdapter(fragment)
    private val companiesRetrieved = PublishSubject.create<Boolean>()

    init {
        dataManager.accessToken?.let {
            httpClient.putAccesToken(it)
        }
    }

    override fun onStart() {
        super.onStart()
        checkCompaniesExist()
        subs += companiesRetrieved.subscribe{
            checkConversationsRetrieved()
        }
    }

    private fun checkConversationsRetrieved() {
        if (dataManager.conversations.isEmpty()) {
            httpClient.getConversations(context, dataManager.companies[0].id, object : HandlerCallback<BaseResponse<ConversationsResponse>>() {
                override fun onError(error: ErrorEntity) {
                    Logger.errorToast(context, error.errorDesc + " " + error.errorCode)
                }

                override fun onComplite(result: BaseResponse<ConversationsResponse>) {
                    if (result.data!=null && !result.data!!.isEmpty()) {
                        dataManager.conversations.clear()
                        dataManager.conversations.addAll(result.data!!)
                        dataManager.conversationsRetrieved.onNext(true)
                    } else {
                        Logger.errorToast(context, "empty conversations")
                    }
                }
            })
        } else {
            dataManager.conversationsRetrieved.onNext(true)
        }
    }

    private fun checkCompaniesExist() {
        if (dataManager.companies.isEmpty()) {
            httpClient.getCompanies(context, object : HandlerCallback<BaseResponse<CompanyesResponse>>() {
                override fun onError(error: ErrorEntity) {
                    Logger.errorToast(context, error.errorDesc + " " + error.errorCode)
                }

                override fun onComplite(result: BaseResponse<CompanyesResponse>) {
                    if (result.data!=null && !result.data!!.isEmpty()) {
                        dataManager.companies.clear()
                        dataManager.companies.addAll(result.data!!)
                        companiesRetrieved.onNext(true)
                    } else {
                        Logger.errorToast(context, "empty companies")
                    }
                }
            })
        } else {
            companiesRetrieved.onNext(true)
        }
    }
}