package com.osome.ui.mainscreen

import android.support.v4.app.Fragment
import android.view.View
import android.widget.TextView
import com.osome.ui.BaseViewModelFragment
import com.osome.ui.chat.ChatFragment
import com.osome.ui.leftmenu.LeftMenuFragment
import com.osome.ui.rightmenu.RightMenuFragment
import com.osome.ui.views.adapters.TabbedPagerAdapter

class MainScreenPagerAdapter(val fragment : BaseViewModelFragment): TabbedPagerAdapter(fragment.fragmentManager!!) {


    val FRAGMENTS_COUNT = 3
    val MAIN_PAGE = 1
    val rightMenuFragment = RightMenuFragment()
    val chatFragment = ChatFragment()
    val leftMenuFragment = LeftMenuFragment()
    val context = fragment.ac()

    override fun getItem(position: Int): Fragment {
        when (position) {
            1 -> return chatFragment
            2 -> return rightMenuFragment
            else -> return leftMenuFragment
        }
    }

    override fun getCount(): Int {
        return FRAGMENTS_COUNT
    }

    override fun getPageTitle(position: Int): CharSequence {
        return ""
    }

    override fun getTabView(position: Int): View {
//        val view = LayoutInflater.from(context).inflate(R.layout.main_pager_tab, null)
//        (view as TextView).text = texts[position]
        val view = TextView(context)
        view.text = position.toString()
        return view
    }

    override fun getOffscreenPageLimit(): Int {
        return FRAGMENTS_COUNT
    }

    override fun getPageWidth(position: Int): Float {
        if (position == MAIN_PAGE) {
            return 1.05f
        } else {
            return 0.8f
        }
    }

    override fun getStartPage(): Int {
        return MAIN_PAGE
    }
}