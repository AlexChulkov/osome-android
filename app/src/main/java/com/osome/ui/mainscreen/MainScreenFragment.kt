package com.osome.ui.mainscreen

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.osome.R
import com.osome.databinding.FragmentMainBinding
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment

class MainScreenFragment: BaseViewModelFragment() {

    lateinit var binding: FragmentMainBinding
    var mainFragmentViewModel: MainScreenFragmentViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_main, container, false)
        return binding.root
    }

    override fun createViewModelAndBind(): BaseViewModel {
        if (mainFragmentViewModel==null) {
            mainFragmentViewModel = MainScreenFragmentViewModel(this)
        }
        binding.viewModel = mainFragmentViewModel
        return mainFragmentViewModel!!
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding.unbind()
    }

}