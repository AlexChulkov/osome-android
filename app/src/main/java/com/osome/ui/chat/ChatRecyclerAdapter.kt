package com.osome.ui.chat

import android.databinding.DataBindingUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.osome.R
import com.osome.databinding.LayoutChatSimpleItemBinding
import com.sendbird.android.BaseMessage

class ChatRecyclerAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val SIMPLE_MESSAGE : Int = R.layout.layout_chat_simple_item
    }

    private val dataArray = ArrayList<BaseMessage>()

    fun setData(data: List<BaseMessage>) {
        dataArray.clear()
        dataArray.addAll(data)
        notifyDataSetChanged()
    }

    override fun getItemViewType(position: Int): Int {
        return SIMPLE_MESSAGE
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val vh: RecyclerView.ViewHolder
//        if (viewType == SIMPLE_MESSAGE) {
            val binding = DataBindingUtil.inflate<LayoutChatSimpleItemBinding>(layoutInflater, viewType, parent, false)
            vh = SimpleItemViewHolder(binding)
//        } else {
//
//        }
        return vh
    }

    override fun getItemCount(): Int {
        return dataArray.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (getItemViewType(position)== SIMPLE_MESSAGE) {
            (holder as SimpleItemViewHolder).bind(dataArray[position])
        } else {

        }
    }

    inner class SimpleItemViewHolder(private val binding: LayoutChatSimpleItemBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind (data: BaseMessage) {
            binding.data = data
        }
    }

}