package com.osome.ui.chat

import android.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.osome.R
import com.osome.databinding.FragmentChatBinding
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment

class ChatFragment : BaseViewModelFragment() {
    lateinit var binding: FragmentChatBinding
    var chatFragmentViewModel: ChatFragmentViewModel? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.fragment_chat, container, false)
        return binding.root
    }

    override fun createViewModelAndBind(): BaseViewModel {
        if (chatFragmentViewModel == null) {
            chatFragmentViewModel = ChatFragmentViewModel(this)
        }
        binding.viewModel = chatFragmentViewModel
        return chatFragmentViewModel!!
    }
}