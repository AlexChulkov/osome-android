package com.osome.ui.chat

import android.databinding.ObservableBoolean
import com.osome.R
import com.osome.ui.BaseViewModel
import com.osome.ui.BaseViewModelFragment
import com.osome.utils.Logger
import com.sendbird.android.*
import rx.subjects.PublishSubject
import com.sendbird.android.SendBird.ConnectHandler
import com.sendbird.android.SendBird



class ChatFragmentViewModel(fragment: BaseViewModelFragment): BaseViewModel(fragment.ac()) {

    var showChat = ObservableBoolean(false)
    private var channelOpened = PublishSubject.create<Boolean>()
    private var openChannel: OpenChannel? = null
    private var adapter = ChatRecyclerAdapter()
    private var priviousMessageListQuery: PreviousMessageListQuery? = null
    init {
        SendBird.init(context.getString(R.string.sanbird_appid), context)
    }

    override fun onStart() {
        super.onStart()
        subs += dataManager.conversationsRetrieved.distinctUntilChanged().subscribe{
            if (it) {
                SendBird.connect(dataManager.user!!.id.toString(), ConnectHandler { user, e ->
                    if (e != null) {
                        Logger.errorToast(context, "connect " + e.message!!)
                        return@ConnectHandler
                    }
                    getChennel()
                })
            }
        }
        subs += channelOpened.distinctUntilChanged().subscribe {
            if (it) {
                addChennelHandler()
                priviousMessageListQuery = openChannel?.createPreviousMessageListQuery()
                loadMessagesPack()
            } else {
                showChat.set(it)
            }
        }
    }

    private fun getChennel() {
        OpenChannel.getChannel(dataManager.conversations[0].externalID, OpenChannel.OpenChannelGetHandler { openChannel, e ->
            if (e != null) {
                Logger.errorToast(context, "getChannel " + e.message!!)
                return@OpenChannelGetHandler
            }
            openChannel.enter(OpenChannel.OpenChannelEnterHandler { e ->
                if (e != null) {
                    Logger.errorToast(context, "enter " + e.message!!)
                    return@OpenChannelEnterHandler
                }
                this.openChannel = openChannel
                channelOpened.onNext(true)
            })
        })
    }

    private fun loadMessagesPack() {
        priviousMessageListQuery?.load(30, true, PreviousMessageListQuery.MessageListQueryResult { messages, e ->
            if (e != null) {
                Logger.errorToast(context, "load " + e.message!!)
                return@MessageListQueryResult
            }
            showChat.set(true)
            adapter
        })
    }

    private fun addChennelHandler() {
        SendBird.addChannelHandler(dataManager.conversations[0].externalID, object : SendBird.ChannelHandler() {
            override fun onMessageReceived(baseChannel: BaseChannel, baseMessage: BaseMessage) {
                if (baseMessage is UserMessage) {
                    // message is a UserMessage
                } else if (baseMessage is FileMessage) {
                    // message is a FileMessage
                } else if (baseMessage is AdminMessage) {
                    // message is an AdminMessage
                }
            }
        })
    }

}