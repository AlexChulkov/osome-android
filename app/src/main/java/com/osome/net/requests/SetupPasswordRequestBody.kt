package com.osome.net.requests

class SetupPasswordRequestBody {
    var confirmationToken: String = ""
    var password: String = ""
}