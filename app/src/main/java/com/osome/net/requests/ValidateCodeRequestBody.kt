package com.osome.net.requests

class ValidateCodeRequestBody {
    var confirmationToken: String = ""
    var verificationCode: String = ""
}