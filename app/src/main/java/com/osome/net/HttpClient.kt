/*
 * Copyright © ExpertOption Ltd. All rights reserved.
 */

package com.osome.net

import android.content.Context
import android.content.SharedPreferences
import com.osome.R
import com.osome.net.requests.SetupPasswordRequestBody
import com.osome.net.requests.SmsRequestBody
import com.osome.net.requests.ValidateCodeRequestBody
import com.osome.net.responses.*
import com.osome.utils.SharedPreferencesUtils
import de.adorsys.android.securestoragelibrary.SecurePreferences
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class HttpClient {

    private var retrofit : OsomeRequest? = null
    private var accessToken : String? = null

    private fun retrofit(context: Context) : OsomeRequest {
        if (this.retrofit==null){
            createRetrofit(context)
        }
        return this.retrofit!!
    }

    private fun createRetrofit(context: Context) {
        val interceptor = HttpLoggingInterceptor()
        val client = OkHttpClient.Builder().addInterceptor(interceptor).build()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        val retrofit = Retrofit.Builder()
                .baseUrl(context.getString(R.string.main_url))
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()

        this.retrofit = retrofit.create(OsomeRequest::class.java)
    }

    fun putAccesToken (accessToken: String) {
        this.accessToken = accessToken
    }

    fun requestSms(context: Context, phoneNumber: String, handlerCallback: HandlerCallback<BaseResponse<RequestSmsResponse>>) {
        val retrofit = retrofit(context)
        val body = SmsRequestBody()
        body.phoneNumber = phoneNumber
        val call = retrofit.requestSms(body)
        call.enqueue(handlerCallback)
    }

    fun validateSmsCode(context: Context, confirmationToken: String, verificationCode: String, handlerCallback: HandlerCallback<ResponseBody>) {
        val retrofit = retrofit(context)
        val body = ValidateCodeRequestBody()
        body.confirmationToken = confirmationToken
        body.verificationCode = verificationCode
        val call = retrofit.validateSms(body)
        call.enqueue(handlerCallback)
    }

    fun setupPin(context: Context, confirmationToken: String, pin: String, handlerCallback: HandlerCallback<BaseResponse<SetupPinResponse>>) {
        val retrofit = retrofit(context)
        val body = SetupPasswordRequestBody()
        body.confirmationToken = confirmationToken
        body.password = pin
        val call = retrofit.setupPassword(body)
        call.enqueue(handlerCallback)
    }

    fun getCompanies(context: Context, handlerCallback: HandlerCallback<BaseResponse<CompanyesResponse>>) {
        val retrofit = retrofit(context)
        val call = retrofit.getCompanies(accessToken!!)
        call.enqueue(handlerCallback)
    }

    fun getConversations(context: Context, companyId: Int,  handlerCallback: HandlerCallback<BaseResponse<ConversationsResponse>>) {
        val retrofit = retrofit(context)
        val call = retrofit.getConversations(accessToken!!, companyId)
        call.enqueue(handlerCallback)
    }

}
