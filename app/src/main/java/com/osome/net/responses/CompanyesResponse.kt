package com.osome.net.responses

import com.osome.data.Company
import com.osome.net.ApiMessage

class CompanyesResponse : ArrayList<Company>(), ApiMessage