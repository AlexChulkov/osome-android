package com.osome.net.responses

import com.osome.data.Conversation
import com.osome.net.ApiMessage

class ConversationsResponse: ArrayList<Conversation>() , ApiMessage