package com.osome.net.responses

import com.osome.net.ApiMessage

class BaseResponse<Response : ApiMessage>{

    var success : Boolean = false
    var data: Response? = null
}
