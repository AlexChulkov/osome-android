package com.osome.net.responses

import com.osome.net.ApiMessage

class RequestSmsResponse : ApiMessage {
    var confirmationToken : String = ""
}