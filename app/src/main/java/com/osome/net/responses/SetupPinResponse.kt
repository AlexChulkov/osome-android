package com.osome.net.responses

import com.osome.data.User
import com.osome.net.ApiMessage

class SetupPinResponse : ApiMessage {
    lateinit var user : User
    lateinit var token : String
}