/*
 * Copyright © ExpertOption Ltd. All rights reserved.
 */

package com.osome.net

import com.osome.net.requests.SetupPasswordRequestBody
import com.osome.net.requests.SmsRequestBody
import com.osome.net.requests.ValidateCodeRequestBody
import com.osome.net.responses.*
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface OsomeRequest {

    @POST("/api/v1/auth/sms")
    fun requestSms(@Body registrationBody: SmsRequestBody) : Call<BaseResponse<RequestSmsResponse>>

    @POST("/api/v1/auth/sms/validate")
    fun validateSms(@Body registrationBody: ValidateCodeRequestBody) : Call<ResponseBody>

    @POST("/api/v1/auth/sms/password")
    fun setupPassword(@Body registrationBody: SetupPasswordRequestBody) : Call<BaseResponse<SetupPinResponse>>

    @GET("/api/v1/companies")
    fun getCompanies(@Header("x-access-token") contentRange: String) : Call<BaseResponse<CompanyesResponse>>

    @GET("/api/v1/companies/{companyId}/conversations")
    fun getConversations(@Header("x-access-token") accessToken: String, @Path("companyId") companyId: Int): Call<BaseResponse<ConversationsResponse>>

}
