package com.osome

import android.app.Application
import android.databinding.DataBindingUtil
import com.osome.di.AppComponent
import com.osome.di.AppModule
import com.osome.di.DaggerAppComponent
import com.osome.ui.bindings.ViewModelsBindings


class App : Application() {

    lateinit var osomeComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        DataBindingUtil.setDefaultComponent(ViewModelsBindings())
        osomeComponent = initDagger(this)
    }


    private fun initDagger(app: App): AppComponent =
            DaggerAppComponent.builder().appModule(AppModule(app)).build()
}