package com.osome.data

class Company {

    /*
    * {
 		"id": 863,
 		"companyName": "+79959036782",
 		"companyStatus": null,
 		"createdAt": "2018-05-01T14:59:41.426Z",
 		"dormant": false,
 		"industry": null,
 		"size": 1,
 		"status": "active",
 		"color": "#ef687d",
 		"logo": null,
 		"dealStatus": "new",
 		"pipelineStage": "new",
 		"companyUser": {
 			"id": 1148,
 			"role": 3,
 			"company": 863,
 			"user": 1386,
 			"status": "active",
 			"inviteCode": null,
 			"positions": [],
 			"channelIds": [],
 			"numberOfShares": null,
 			"inviteDate": "Tue May 01 2018 14:59:41 GMT+0000 (UTC)",
 			"inviteUser": null,
 			"businessRelationships": null
 		}
 	}
    * */

    var id : Int = 0
    var companyName : String? = null
    var companyStatus : String? = null
    var createdAt : String? = null
    var dormant: Boolean? = null
    var industry : String? = null
    var size : Int = 0
    var status : String? = null
    var color : String? = null
    var logo: String? = null
    var dealStatus : String? = null
    var pipelineStage : String? = null
    var companyUser : СompanyUser? = null
}