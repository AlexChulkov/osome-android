package com.osome.data

class Conversation {

    /*
    * {
		"id": 1300,
		"name": "+79959036782",
		"externalID": "sendbird_group_channel_61823488_59a5c046784fd89095f74439406d417f38b7a893",
		"forBot": true,
		"company": 863,
		"userId": 1386,
		"changeReadStatus": null,
		"changeAnsweredStatus": null,
		"lastMessage": null,
		"isRead": true,
		"isAnswered": true
	}
    * */

    var id : Int = 0
    var name : String = ""
    var externalID : String? = null
    var forBot : Boolean = false
    var company : Int = 0
    var userId : Int = 0
    var changeReadStatus : String? = null
    var changeAnsweredStatus : String? = null
    var lastMessage : String? = null
    var isRead : Boolean = false
    var isAnswered : Boolean = false
}