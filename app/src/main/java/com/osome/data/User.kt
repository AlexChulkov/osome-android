package com.osome.data

class User {
/* "id": 1386,
	"email": null,
	"phoneNumber": "+79959036782",
	"firstName": null,
	"lastName": null,
	"avatar": null,
	"accessToken": "PeXxMZ0AZwjQLl1lBxH8KJsPfvbnO3SglC0DoNVuISHXU8CmjnUTTSdfQ4nJ",
	"nric": null,
	"nationality": null,
	"nickname": null,
	"documentNumber": null,
	"documentType": null,
	"birthdate": null,
	"sex": null,
	"passedKYC": null,
	"companiesOrder": null,
	"pushInfo": null
}, "accessTokens": {
	"id": 34,
	"userId": 1386,
	"accessToken1": "U2FsdGVkX186sAXKPgQuLD1sKBqGyQZUOK/5sXWPnHGoCliwfSPd5fAEc7ThlmM5leLW3tAnkDnvq95iQ10DXw==",
	"createdAt": "2018-05-01T14:59:41.917Z",
	"updatedAt": "2018-05-01T14:59:42.334Z"
	*/

    var id : Int = 0
    var email : String? = null
    var phoneNumber : String = ""
    var firstName : String? = null
    var lastName : String? = null
    var avatar : String? = null
    var accessToken : String = ""
    var nric : String? = null
    var nationality : String? = null
    var nickname : String? = null
    var documentType : String? = null
    var birthdate : String? = null
    var sex : String? = null
    var passedKYC : String? = null
    var companiesOrder : String? = null
    var pushInfo : String? = null
    var accessTokens : AccessTokens? = null
}