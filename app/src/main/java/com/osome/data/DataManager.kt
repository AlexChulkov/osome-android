package com.osome.data

import android.content.Context
import com.osome.utils.SharedPreferencesUtils
import de.adorsys.android.securestoragelibrary.SecurePreferences
import rx.subjects.PublishSubject

class DataManager (val context: Context) {

    val sharedPreferences = SharedPreferencesUtils(context)
    var authState = AuthState.getAuthStateFromSharedPreferences(sharedPreferences)
    set(authState) {
        sharedPreferences.getSharedPreferencesForEdit(context).putInt(SharedPreferencesUtils.AUTH_STATE, authState.intValue).commit()
        field = authState
    }
    var user: User? = SharedPreferencesUtils.loadUser()
    set(userValue) {
        userValue?.let {
            SharedPreferencesUtils.saveUser(userValue)
        }
        field = userValue
    }

    var accessToken: String? = SecurePreferences.getStringValue(SharedPreferencesUtils.TOKEN, null)
    set(accessToken) {
        accessToken?.let {
            SecurePreferences.setValue(SharedPreferencesUtils.TOKEN, accessToken)
        }
        field = accessToken
    }

    var companies = ArrayList<Company>()
    val conversations = ArrayList<Conversation>()
    val conversationsRetrieved = PublishSubject.create<Boolean>()

    enum class AuthState(val intValue: Int) {
        STATE_FIRST_AUTH(0),
        STATE_REGISTERED(1);

        companion object {
            fun getAuthStateFromSharedPreferences(sharedPreferences: SharedPreferencesUtils) : AuthState{
                return when (sharedPreferences.getSharedPreferences().getInt(SharedPreferencesUtils.AUTH_STATE, 0)){
                    1 -> AuthState.STATE_REGISTERED
                    else -> AuthState.STATE_FIRST_AUTH
                }
            }
        }
    }
}