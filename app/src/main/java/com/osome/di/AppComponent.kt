package com.osome.di

import com.osome.ui.BaseViewModel
import com.osome.ui.RouterActivity
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = [ AppModule::class ])
interface AppComponent {

    @SuppressWarnings("LeakingThisInConstructor")
    fun inject(baseViewModel: BaseViewModel)

    @SuppressWarnings("LeakingThisInConstructor")
    fun inject(activity: RouterActivity)

}