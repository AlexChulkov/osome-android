package com.osome.di

import android.content.Context
import com.osome.net.HttpClient
import com.osome.App
import com.osome.data.DataManager
import dagger.Module
import javax.inject.Singleton
import dagger.Provides

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

    @Provides
    @Singleton
    fun provideDataManager(): DataManager = DataManager(app)

    @Provides
    @Singleton
    fun provideHttpClient(): HttpClient = HttpClient()
}